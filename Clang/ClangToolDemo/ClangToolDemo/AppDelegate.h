//
//  AppDelegate.h
//  ClangToolDemo
//
//  Created by aron on 2019/10/9.
//  Copyright © 2019 aron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

