//
//  AppDelegate.m
//  ClangToolDemo
//
//  Created by aron on 2019/10/9.
//  Copyright © 2019 aron. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
@property (nonatomic, copy) NSString *goodString;
@property (nonatomic, strong) NSString *badString;
@end

@implementation AppDelegate

@end
