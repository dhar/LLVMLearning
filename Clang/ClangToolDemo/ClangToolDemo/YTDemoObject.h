//
//  YTDemoObject.h
//  ClangToolDemo
//
//  Created by aron on 2019/10/9.
//  Copyright © 2019 aron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    BadEnumType1,
} BadEnum;

@interface YTDemoObject : NSObject {
    NSString *_badValue;
}

@property (nonatomic, copy) NSString *goodString;
@property (nonatomic, strong) NSString *badString;
@property (nonatomic, strong) id<UITableViewDelegate> badDelegate;
@property (atomic, strong) id badAtomicProp;
@property (nonatomic, strong) id BadProp;

@end

@interface badInterface : NSObject
- (void)BadMethod;
@end
