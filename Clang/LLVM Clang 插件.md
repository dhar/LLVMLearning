Clang插件分享

## LLVM Clang安装

**Checkout LLVM:**

- cd 到放 LLVM 的路径下
-  git clone https://git.llvm.org/git/llvm.git/  

**Checkout Clang:**
- cd llvm/tools
- git clone https://git.llvm.org/git/clang.git/  

**生成Xcode项目**

-  cd 到放 LLVM 的路径下，创建一个编译xcode项目的文件夹 llvm-xcode-build
- 进入到创建的文件夹`cd llvm-xcode-build`
- 使用cmake生成Xcode项目 `cmake -G Xcode ../llvm/ -DCMAKE_BUILD_TYPE=Release`，`DCMAKE_BUILD_TYPE=Release`这个是编译生成产物的选项，默认是Debug模式，生成的结果会比较大，有20G左右，推荐使用Release  
- 生成Xcode项目如下所示  

![image-20191009203948095](https://tva1.sinaimg.cn/large/006y8mN6gy1g7s8ctdoh3j30bj0a1gnh.jpg)  

- 选择自动创建Schemes  

![image-20191009204417094](https://tva1.sinaimg.cn/large/006y8mN6gy1g7s8hfe96gj30g8050wgj.jpg)  

- `cmd+B`进行编译，编译最终会在Products目录下生成llvm、clang的命令行工具，这些工具我们后面会使用到    

![image-20191009204615489](https://tva1.sinaimg.cn/large/006y8mN6gy1g7s8ji2qi4j30av082gnd.jpg)  

## Clang插件

### 插件效果

一个简单的的检测`NSString*`类型的属性的修饰符是否是`copy`的插件的运行效果

![image-20191009213930989](https://tva1.sinaimg.cn/large/006y8mN6gy1g7sa2wtq8yj30lg05iacv.jpg)  

### 抽象语法树

Clang插件是基于AST（抽象语法树）的，所以需要先对抽象语法树做个简单的了解。创建Demo类如下，包含了两个属性，一个是`copy`修饰的`goodString`，另一个是`strong`修饰的`badString`

```
 ClangToolDemo git:(master) ✗ cat YTDemoObject.h
//
//  YTDemoObject.h
//  ClangToolDemo
//
//  Created by aron on 2019/10/9.
//  Copyright © 2019 aron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YTDemoObject : NSObject

@property (nonatomic, copy) NSString *goodString;
@property (nonatomic, strong) NSString *badString;

@end
```

使用下面的代码分析得到语法树

```
clang -fmodules -fsyntax-only -Xclang -ast-dump YTDemoObject.m
```

![image-20191009215823800](https://tva1.sinaimg.cn/large/006y8mN6gy1g7samju6t6j30ms04mq8d.jpg)  

比较重要的是`ObjCPropertyDecl`这个类型，后面我们会使用到该类型，用于属性节点的获取，关于更多的AST节点类型可以查看官方的这个文档 [AST Matcher Reference](http://clang.llvm.org/docs/LibASTMatchersReference.html)  

### 几个重要的类

了解的AST的属性节点，接下来要做的工作就是使用clang的插件解析AST的属性节点，会涉及到几个重要的类

- CompilerInstance

  是一个编译器实例，包含了`ASTContext`（表示某个源文件的AST）、`Preprocessor`、`Diagnostics` 等信息，这个例子中会使用 `Diagnostics` 进行提示信息的输出

  

- PluginASTAction

  是一个基于 `ASTConsumer` 的AST前端 Action 抽象基类，是调用`ASTConsumer`的入口，需要重载`CreateASTConsumer`返回自定义的`Consumer`读取AST

  

- ASTConsumer
  
  是一个读取抽象语法树的抽象基类，提供了多个个方法用于重载，比如用于处理整个AST加载完成的方法`HandleTranslationUnit()`、处理顶层声明（全局变量，函数定义）的方法`HandleTopLevelDecl()`、处理`TagDecl`类型(struct, union, enum)声明的方法`HandleTagDeclDefinition()`，这个例子中会重写`HandleTranslationUnit()`这个方法处理AST
  
  


- MatchFinder

  AST 节点的查找过滤匹配器，提供`addMatcher`方法需要处理的AST节点类型，需要设置一个`MatchFinder::MatchCallback`类型回调处理器，在AST解析完成的`HandleTranslationUnit`方法调用`MatchFinder`的`matchAST`方法，之后会调用回调处理器的`run`方法

  

- MatchFinder::MatchCallback

  回调处理器，重写`run`处理匹配到的结果，参数是`MatchResult`类型，可以获取到具体的AST节点类型


### 插件实现

以一个简单的属性语法检测为例[YTSimpleCodeCheckTool](https://gitee.com/dhar/LLVMLearning/tree/master/Clang/YTSimpleCodeCheckTool)，以下是关键步骤的代码

```c++
// 这里的namespace是集成到Xcode需要使用到的
namespace YTSimpleCodeCheckTool {
    
    class YTMatchHandler: public MatchFinder::MatchCallback {
    private:
        CompilerInstance &CI;
        // 处理ObjCPropertyDecl类型的AST节点，该方法演示了是NSString类型的修饰符是否为copy，如果不是使用CompilerInstance->DiagnosticsEngine进行提示，设置的级别为DiagnosticsEngine::Warning，会在Xcode中有个黄色的提示信息
        void handlePropertyDecl(const ObjCPropertyDecl *propertyDecl) {
            ObjCPropertyDecl::PropertyAttributeKind attrKind = propertyDecl->getPropertyAttributes();
            const string typeStr = propertyDecl->getType().getAsString();//NSString
            
            if (propertyDecl->getTypeSourceInfo()) {
                DiagnosticsEngine &diag = CI.getDiagnostics();
                if (isShouldUseCopy(typeStr)) {
                    if (!(attrKind & ObjCPropertyDecl::OBJC_PR_copy)) {
                        // 使用DiagnosticsEngine进行提示，设置的级别为DiagnosticsEngine::Warning，会在Xcode中有个黄色的提示信息，设置Error以上的级别会是一个红色的编译错误
                        diag.Report(propertyDecl->getBeginLoc(), diag.getCustomDiagID(DiagnosticsEngine::Warning, "❌ ⚠️ YT WARNING %0 应该使用 copy 修饰")) << typeStr;
                    }
                }
            }
        }
       
    public:
        YTMatchHandler(CompilerInstance &CI) :CI(CI) {}
        
        // 处理匹配的结果
        void run(const MatchFinder::MatchResult &Result) {
            // 获取匹配到的的ObjCPropertyDecl类型的AST节点
            const ObjCPropertyDecl *propertyDecl = Result.Nodes.getNodeAs<ObjCPropertyDecl>("objcPropertyDecl");
            // 处理AST节点
            if (propertyDecl &&
                isUserSourceCode(CI.getSourceManager().getFilename(propertyDecl->getSourceRange().getBegin()).str())) {
                handlePropertyDecl(propertyDecl);
            }
        }
    };
    
    class YTASTConsumer: public ASTConsumer {
    private:
        MatchFinder matcher;
        YTMatchHandler handler;
    public:
        YTASTConsumer(CompilerInstance &CI) :handler(CI) {
            // 添加需要匹配的类型，这里添加了OC中的属性类型为objcPropertyDecl()，标识符为"objcPropertyDecl"，这个表示如需要和`handler`中的getNodeAs<ObjCPropertyDecl>("objcPropertyDecl")标识符一致才能取到AST节点
            matcher.addMatcher(objcPropertyDecl().bind("objcPropertyDecl"), &handler);
        }
        // AST解析完成之后的回调方法，ASTContext保存了AST的所有信息，调用MatchFinder的matchAST方法获取匹配的结果，会调用YTMatchHandler的run处理结果
        void HandleTranslationUnit(ASTContext &context) {
            matcher.matchAST(context);
        }
    };
    
    // 调用ASTConsumer的入口，重写CreateASTConsumer方法返回自定义的ASTConsumer
    class YTASTAction: public PluginASTAction {
    public:
        unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef iFile) {
            return unique_ptr<YTASTConsumer> (new YTASTConsumer(CI));
        }
    };
}

// 注册PluginASTAction
static FrontendPluginRegistry::Add<YTSimpleCodeCheckTool::YTASTAction> X("YTSimpleCodeCheckTool", "A custom static code analysis of Objc");		
```

把代码放到llvm工程的`tools/clang/tools`目录下

![image-20191011110432463](https://tva1.sinaimg.cn/large/006y8mN6ly1g7u2yuz9k2j30km08ldjf.jpg)  



编写`CMakeList.txt`文件如下

```
add_llvm_library(YTSimpleCodeCheckTool MODULE 
	YTSimpleCodeCheckTool.cpp
	PLUGIN_TOOL
	clang
)

if(LLVM_ENABLE_PLUGINS AND (WIN32 OR CYGWIN))
  target_link_libraries(YTSimpleCodeCheckTool PRIVATE
    clangAST
    clangBasic
    clangFrontend
    LLVMSupport
    )
endif()
```

修改llvm工程的`tools/clang/tools/CMakeList.txt`文件，在末尾添加

```
# 添加自定义插件文件夹
add_clang_subdirectory(YTSimpleCodeCheckTool)
```

然后重新运行构建命令 `cmake -G Xcode ../llvm/ -DCMAKE_BUILD_TYPE=Release` ，重新打开Xcode项目，可以看到加入了我们的插件代码，`cmd+B`之后会在Product组下面生成了插件`YTSimpleCodeCheckTool.dylib`，接下来我们会把插件继承到项目中

![image-20191011111314096](https://tva1.sinaimg.cn/large/006y8mN6ly1g7u37w5n46j30s20a2ago.jpg)

### 插件集成到项目中

- 配置 Other C Flags  

```
-Xclang -load -Xclang (.dylib)动态库路径 -Xclang -add-plugin -Xclang 插件名字（namespace 的名字，名字不对则无法使用插件）

-Xclang -load -Xclang /Users/aron/LLVM/llvm-xcode-build/Debug/lib/YTSimpleCodeCheckTool.dylib -Xclang -add-plugin -Xclang YTSimpleCodeCheckTool
```

![image-20191009212812014](https://tva1.sinaimg.cn/large/006y8mN6gy1g7s9r4x4yqj30ig03fgmm.jpg)  

- Xcode添加用于自定义配置，配置自己编译的Clang用于加载插件  

![image-20191009213227455](https://tva1.sinaimg.cn/large/006y8mN6gy1g7s9vk8ibsj30it037my2.jpg) 

- 修改`Enable Index-While-Building Functionality`为`NO`  
![image-20191009213700130](https://tva1.sinaimg.cn/large/006y8mN6gy1g7sa0ahpaqj30h302vaa9.jpg) 

- 结果

![image-20191009213930989](https://tva1.sinaimg.cn/large/006y8mN6gy1g7sa2wtq8yj30lg05iacv.jpg)

  ### 一个更加完整的代码检测插件

基于以上的步骤，可以实现一个`enum`、`Interface/Class`、`Method`、`Atomic`等语法检查的插件[YTCodeCheckTool](https://gitee.com/dhar/LLVMLearning/tree/master/Clang/YTCodeCheckTool)，效果如下所示  

![image-20191011103323811](https://tva1.sinaimg.cn/large/006y8mN6ly1g7u22iuyluj30qr0cnak6.jpg)  

## 参考资料

- [LLVM Cookbook中文版](https://book.douban.com/subject/26820613/)  
- [LLVM编译器实战教程](https://book.douban.com/subject/34802579/)  
- [极客时间的付费课程-【编译原理之美】](https://time.geekbang.org/column/intro/219)  