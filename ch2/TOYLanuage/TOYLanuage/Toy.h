//
//  Toy.h
//  TOYLanuage
//
//  Created by aron on 2019/9/26.
//  Copyright © 2019 aron. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 EOF_TOKEN： 它规定文件的结束。
 NUMBERIC_TOKEN： 当前token是数值类型的。
 IDENTIFIER_TOKEN： 当前token是标识符。
 PAREN_TOKEN： 当前token是括号。
 DEF_TOKEN： 当前token是def声明， 之后是函数定义。
 */
enum Token_Type {
    EOF_TOKEN = 0,
    NUMBERIC_TOKEN,
    IDENTIFIER_TOKEN,
    PAREN_TOKEN,
    DEF_TOKEN,
};

@interface Toy : NSObject

@end
