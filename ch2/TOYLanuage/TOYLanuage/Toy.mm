//
//  Toy.m
//  TOYLanuage
//
//  Created by aron on 2019/9/26.
//  Copyright © 2019 aron. All rights reserved.
//

#import "Toy.h"
#include <string>

static int Numeric_Val;
static std::string Identifier_string;

static int get_token(FILE *file) {
    static int LastChar = ' ';
    
    while (isspace(LastChar)) {
        LastChar = fgetc(file);
    }
    
    if (isalpha(LastChar)) {
        Identifier_string = LastChar;
        while (isalnum((LastChar = fgetc(file)))) {
            Identifier_string += LastChar;
        }
        
        if (Identifier_string == "def") {
            return DEF_TOKEN;
        }
        
        return IDENTIFIER_TOKEN;
    }
    
    if (isdigit(LastChar)) {
        std::string NumStr;
        do {
            NumStr += LastChar;
            LastChar = fgetc(file);
        } while (isdigit(LastChar));
        Numeric_Val = strtod(NumStr.c_str(), 0);
        return NUMBERIC_TOKEN;
    }
    
    if (LastChar == '#') {
        do {
            LastChar = fgetc(file);
        } while (LastChar != EOF && LastChar != '\n');
        if (LastChar != EOF) {
            return get_token(file);
        }
    }
    
    if (LastChar == EOF) {
        return EOF_TOKEN;
    }
    
    int ThisChar = LastChar;
    LastChar = fgetc(file);
    
    return ThisChar;
}

@implementation Toy

@end
